﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BinaryTree
{
    public class BinaryTree<T> : IBinaryTree<T>
    {
        public event EventHandler ElementAdd;
        public event EventHandler ElementRemove;

        public int Count { get; private set; }

        //TODO: Create private readonly delegate field (custom or from library) that will be tree comparer
        //Parameters: T, T
        //Returns: int
        

        public BinaryTree()
        {
            //TODO: Check if type T implements IComparable<T>
            //If it does: Write a lambda function, that calls T.CompateTo(T) method, in delegate field created before
            //If doesn't: Throw exception
        }

        public BinaryTree(IComparer<T> comparer)
        {
            //TODO: Write method of comparing to a delegate field created before

            
        }

        public void Add(T item)
        {
            //TODO: Add element according to comparer
            //Call event that element was added

            throw new NotImplementedException();
        }

        public bool Remove(T item)
        {
            //TODO: Remove element from tree
            //Return false if element wasn't found in tree
            //Return true if element was in tree and was deleted succesfully
            //Call event if element was removed

            throw new NotImplementedException();
        }

        public bool Contains(T item)
        {
            //TODO: Check if tree contains element
            //Return true if does
            //Return false if doesn't

            throw new NotImplementedException();
        }

        public T TreeMax()
        {
            //TODO: Return item with the highest value according to comparer

            throw new NotImplementedException();
        }

        public T TreeMin()
        {
            //TODO: Return item with the lowest value according to comparer

            throw new NotImplementedException();
        }

        public IEnumerator<T> GetEnumerator()
        {
            //TODO: Make in-order traverse using yield

            throw new NotImplementedException();
        }

        public IEnumerable<T> Traverse(TraverseType traverseType)
        {
            //TODO: Return a sequense of elements of tree according to traverse type

            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            //TODO: Make in-order traverse using yield

            throw new NotImplementedException();
        }
    }

    public enum TraverseType
    {
        InOrder,
        PreOrder,
        OutOrder
    }
}
