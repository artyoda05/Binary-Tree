﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinaryTree
{
    public interface IBinaryTree<T> : IEnumerable<T>
    {
        event EventHandler ElementAdd;

        event EventHandler ElementRemove;

        int Count { get; }

        void Add(T item);

        bool Remove(T item);

        bool Contains(T item);

        T TreeMax();

        T TreeMin();

        IEnumerable<T> Traverse(TraverseType traverseType);
    }
}
